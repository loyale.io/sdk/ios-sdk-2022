//
//  Country.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public struct Country {

    public let id: String
    public let name: String
    public let nativeNames: [String]
    public let alpha2Code: String
    public let alpha3Code: String
    public let callingCode: String
    public let currencyName: String
    public let currencyCode: String?
    public let currencySymbol: String?
    public let languageNames: [String]
    public let languageCodes: [String]
}

extension Country: Decodable {
   
    enum CodingKeys: String, CodingKey {
       case id
       case name
       case nativeNames
       case alpha2Code
       case alpha3Code
       case callingCode
       case currencyName
       case currencyCode
       case currencySymbol
       case languageNames
       case languageCodes
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.nativeNames = try container.decode([String].self, forKey: .nativeNames)
        self.alpha2Code = try container.decode(String.self, forKey: .alpha2Code)
        self.alpha3Code = try container.decode(String.self, forKey: .alpha3Code)
        self.callingCode = try container.decode(String.self, forKey: .callingCode)
        self.currencyName = try container.decode(String.self, forKey: .currencyName)
        self.currencyCode = try? container.decode(String.self, forKey: .currencyCode)
        self.currencySymbol = try? container.decode(String.self, forKey: .currencySymbol)
        self.languageNames = try container.decode([String].self, forKey: .languageNames)
        self.languageCodes = try container.decode([String].self, forKey: .languageCodes)
    }
}
