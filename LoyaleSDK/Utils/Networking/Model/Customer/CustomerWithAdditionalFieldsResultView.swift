//
//  CustomerWithAdditionalFieldsResultView.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public class CustomerWithAdditionalFieldsResultView: Decodable {
    
    public var customer: CustomerResultView
    public var additionalFields: [AdditionalCustomerFieldsResultView]
    public var updatedDate: String?
    public var createdDate: String?

    private enum CodingKeys: String, CodingKey {
        case customer
        case additionalFields
        case updatedDate
        case createdDate
    }
    
    required public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.customer = try container.decode(CustomerResultView.self, forKey: .customer)
        self.additionalFields = try container.decode([AdditionalCustomerFieldsResultView].self, forKey: .additionalFields)
        self.updatedDate = try container.decode(String.self, forKey: .updatedDate)
        self.createdDate = try container.decode(String.self, forKey: .createdDate)
    }
}
