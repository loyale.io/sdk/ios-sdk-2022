//
//  PointBalanceResultView.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public struct PointBalanceResultView {
    
    public let id: String
    public let customerId: String?
    public let schemeId: String?
    public let value: Double?
    public let pointsValue: Double
    public let monetaryValue: Double
    public let history: [Double]?
    public let updatedDate: String?
    public let createdDate: String?
}

extension PointBalanceResultView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case customerId
        case schemeId
        case value
        case pointsValue
        case monetaryValue
        case history
        case updatedDate
        case createdDate
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.id = try container.decode(String.self, forKey: .id)
        self.customerId = try? container.decode(String.self, forKey: .customerId)
        self.schemeId = try? container.decode(String.self, forKey: .schemeId)
        self.value = try? container.decode(Double.self, forKey: .value)
        self.pointsValue = try container.decode(Double.self, forKey: .pointsValue)
        self.monetaryValue = try container.decode(Double.self, forKey: .monetaryValue)
        self.history = try? container.decode([Double].self, forKey: .history)
        self.updatedDate = try? container.decode(String.self, forKey: .updatedDate)
        self.createdDate = try? container.decode(String.self, forKey: .createdDate)
    }
    
}
