//
//  FranchiseEndpoint.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public enum FranchiseEndpoint {
    case getFranchises(filters: String?, sorts: String?, page: Int?, pageSize: Int?)
    case getFranchiseById(_ id: String)
}

extension FranchiseEndpoint: EndPointType {
    
    public var loyaleToken: String {
        return "Bearer \(LoyaleConfig.shared.token)"
    }
    
    public var schemeId: String {
        return LoyaleConfig.shared.schemeId
    }
    
    public var baseURL: URL {
        return LoyaleConfig.shared.baseLoyaltyURL
    }
    
    public var path: String {
        switch self {
        case .getFranchises:
            return "api/Franchise"
        case .getFranchiseById(let franchiseId):
            return "api/Franchise/\(franchiseId)"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .getFranchises:
            return .get
        case .getFranchiseById:
            return .get
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .getFranchises(let filters, let sorts, let page, let pageSize):
            var params = Parameters()
            filters != nil ? params["Filters"] = filters! : nil
            sorts != nil ? params["Sorts"] = sorts! : nil
            page != nil ? params["Page"] = page : nil
            pageSize != nil ? params["PageSize"] = pageSize! : nil
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: params , additionHeaders: headers)
        case .getFranchiseById:
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: headers)
        }
    }
    
    public var headers: HTTPHeaders? {
        switch self {
        default:
            return ["Scheme": schemeId, "Authorization": loyaleToken]
        }
    }
}

