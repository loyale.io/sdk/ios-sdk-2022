//
//  LevelResultView.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public struct LevelResultView {
    
    public let id: String
    public let tierId: String
    public let schemeId: String?
    public let name: String
    public let pointAllocationPerCurrency: Double
    public let threshold: Int
    public let joinByQRCode: Bool?
    public let percentageDiscount: Double
    public let externalRef: String?
}

extension LevelResultView {
    public init(name: String, id: String) {
        self.id = id
        self.tierId = ""
        self.schemeId = ""
        self.name = name
        self.pointAllocationPerCurrency = 0
        self.threshold = 0
        self.joinByQRCode = false
        self.percentageDiscount = 0
        self.externalRef = nil
    }
}

extension LevelResultView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case tierId
        case schemeId
        case name
        case pointAllocationPerCurrency
        case threshold
        case joinByQRCode
        case percentageDiscount
        case externalRef
    }
    
    public init(from decoder: Decoder) throws {
       
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.tierId = try container.decode(String.self, forKey: .tierId)
        self.percentageDiscount = try container.decode(Double.self, forKey: .percentageDiscount)
        self.name = try container.decode(String.self, forKey: .name)
        self.pointAllocationPerCurrency = try container.decode(Double.self, forKey: .pointAllocationPerCurrency)
        self.threshold = try container.decode(Int.self, forKey: .threshold)
        self.joinByQRCode = try? container.decode(Bool.self, forKey: .joinByQRCode)
        self.externalRef = try? container.decode(String.self, forKey: .externalRef)
        self.schemeId = try? container.decode(String.self, forKey: .schemeId)
    }
}
