//
//  SchemeRouter.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public class SchemeRouter {
    
    private let router = Router<SchemeEndpoint>()
    
    public func getScheme(byId id: String, completion: @escaping (_ model: SchemeResultView?, _ error: String?)->()) {
        router.getData(.getSchemeById(id)) { (model: SchemeResultView?, error: String?) in
            completion(model, error)
        }
    }
}
