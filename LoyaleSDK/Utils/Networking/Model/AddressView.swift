//
//  AddressView.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public struct AddressView {
    
    public var addressLine1: String?
    public var addressLine2:  String?
    public var town: String?
    public var postCode: String?
    public var state: String?
    public var countryId: String?
}

extension AddressView: Decodable {
   
    private enum CodingKeys: String, CodingKey {
        case addressLine1
        case addressLine2
        case town
        case postCode
        case state
        case countryId
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.addressLine1 = try? container.decode(String.self, forKey: .addressLine1)
        self.addressLine2 = try? container.decode(String.self, forKey: .addressLine2)
        self.town = try? container.decode(String.self, forKey: .town)
        self.postCode = try? container.decode(String.self, forKey: .postCode)
        self.state = try? container.decode(String.self, forKey: .state)
        self.countryId = try? container.decode(String.self, forKey: .countryId)
    }
}
