//
//  CustomerInsertView.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public class CustomerInsertView: BaseCustomer {
    
    public var password: String?
    public var referralCode: String?
    public var skipMailgunValidation: Bool?
    public var skipMobileUniqueValidation: Bool?
    public var parentId: String?
    public var device: CustomerDevice?
    
    override public init() {
        super.init()
    }
    
    required public init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    private enum CodingKeys: String, CodingKey {
        case password
        case referralCode
        case skipMailgunValidation
        case skipMobileUniqueValidation
        case parentId
        case device
    }
    
    public override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(self.password, forKey: .password)
        try? container.encode(self.referralCode, forKey: .referralCode)
        try? container.encode(self.skipMailgunValidation, forKey: .skipMailgunValidation)
        try? container.encode(self.skipMobileUniqueValidation, forKey: .skipMobileUniqueValidation)
        try? container.encode(self.parentId, forKey: .parentId)
        try? container.encode(self.device, forKey: .device)
        try super.encode(to: encoder)
    }
}

extension CustomerInsertView {
    
    public func parameters() -> Parameters {
        do{
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let mappedUser = try? encoder.encode(self)
            let json = try JSONSerialization.jsonObject(with: mappedUser!, options: .allowFragments) as? [String : Any]
            return json ?? [String: Any]()
        } catch {
            print(error)
            return [String: Any]()
        }
    }
}
