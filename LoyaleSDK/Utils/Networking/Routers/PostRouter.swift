//
//  PostRouter.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public class PostRouter {
    
    private let router = Router<PostEndpoint>()
    
    public func getAlerts(filteredBy filters: String?, sorts: String?, page: Int?, pageSize: Int?, completion: @escaping (_ model: [AlertLinked]?, _ error: String?)->()) {
        router.getData(.getAlerts(filters: filters, sorts: sorts, page: page, pageSize: pageSize)) { (model: [AlertLinked]?, error: String?) in
            completion(model, error)
        }
    }
    
    public func getAlert(byId id: String, completion: @escaping (_ model: AlertLinked?, _ error: String?)->()) {
        router.getData(.getAlertById(id)) { (model: AlertLinked?, error: String?) in
            completion(model, error)
        }
    }
}
