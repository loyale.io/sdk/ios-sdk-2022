//
//  MobileCode.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public struct MobileCode {
    
    public let code: String
    public let iso: String
    public let country: String
}

extension MobileCode: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case code
        case iso
        case country
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.code = try container.decode(String.self, forKey: .code)
        self.iso = try container.decode(String.self, forKey: .iso)
        self.country = try container.decode(String.self, forKey: .country)
    }
}
