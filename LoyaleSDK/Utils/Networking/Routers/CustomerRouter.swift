//
//  CustomerRouter.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import UIKit

public class CustomerRouter {
    
    private let router = Router<CustomerEndpoint>()
    
//case uploadProfilePicture
    
    public func createCustomer(_ customer: CustomerInsertView, additionalFiedlds: [AdditionalCustomerFieldsResultView], completion: @escaping (_ model: CustomerResultView?, _ error: String?)->()) {
        router.getData(.createCustomer(customer: customer, additionalFiedlds: additionalFiedlds)) { (model: CustomerResultView?, error: String?) in
            completion(model, error)
        }
    }
    
    public func updateCustomer(_ customer: CustomerResultView, completion: @escaping (_ model: CustomerResultView?, _ error: String?)->()) {
        router.request(.updateCustomer(customer: customer)) { data, response, error in
            if error != nil {
                completion(nil, "Please check your network connection.")
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.router.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else { completion(nil, NetworkResponse.noData.rawValue); return }
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        print(jsonData)
                        guard let json = jsonData as? [String: Any], let customerJson = json["customer"] as? [String: Any] else { completion(nil, NetworkResponse.unableToDecode.rawValue); return }
                        let model = try CustomerResultView.self(from: customerJson)
                        completion(model, nil)
                    } catch {
                        print(error)
                        completion(nil, error.localizedDescription)
                    }
                case .failure(let networkFailureError):
                    completion(nil, self.router.errorString(fromData: data) ?? networkFailureError)
                }
            }
        }
    }
    
    public func getCustomer(byId id: String, completion: @escaping (_ model: CustomerResultView?, _ error: String?)->()) {
        router.getData(.getCustomerById(id)) { (model: CustomerResultView?, error: String?) in
            completion(model, error)
        }
    }
    
    public func getCustomerWithAdditional(customerId id: String, completion: @escaping (_ model: CustomerWithAdditionalFieldsResultView?, _ error: String?)->()) {
        router.getData(.getСustomerWithAdditional(customerId: id)) { (model: CustomerWithAdditionalFieldsResultView?, error: String?) in
            completion(model, error)
        }
    }
    
    public func postFirebaseToken(_ token: String, forCustomerWithId id: String, completion: @escaping (_ success: Bool, _ error: String?)->()) {
        router.simpleRequest(.postFirebaseToken(token: token, customerId: id)) { succes, error in
            completion(succes, error)
        }
    }
    
    public func deleteFirebaseToken(_ token: String, completion: @escaping (_ success: Bool, _ error: String?)->()) {
        router.simpleRequest(.deleteFirebaseToken(token: token)) { succes, error in
            completion(succes, error)
        }
    }
    
    public func uploadProfilePicture(_ picture: UIImage, customerId: String, completion: @escaping (_ success: Bool, _ error: String?)->()) {
        let boundary = "Boundary-\(UUID().uuidString)"
        router.uploadImage(.uploadProfilePicture(customerId: customerId, boundary: boundary), boundary: boundary, image: picture) { succes, error in
            completion(succes, error)
        }
    }
    
    public func leaveScheme(schemeId: String, customerId: String, completion: @escaping (_ success: Bool, _ error: String?)->()) {
        router.simpleRequest(.leaveScheme(schemeId: schemeId, customerId: customerId)) { succes, error in
            completion(succes, error)
        }
    }
    
    public func joinScheme(schemeId: String, customerId: String, completion: @escaping (_ success: Bool, _ error: String?)->()) {
        router.simpleRequest(.joinScheme(schemeId: schemeId, customerId: customerId)) { succes, error in
            completion(succes, error)
        }
    }
    
    public func suspendAccount(customerId id: String, completion: @escaping (_ success: Bool, _ error: String?)->()) {
        router.simpleRequest(.suspend(customerId: id)) { succes, error in
            completion(succes, error)
        }
    }
}
