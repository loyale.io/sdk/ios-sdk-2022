//
//  OtherEndpoint.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public enum OtherEndpoint {
    case postMessage(message: Message)
    case contactMatch(contacts: [String])
}

extension OtherEndpoint: EndPointType {
    
    public var loyaleToken: String {
        return "Bearer \(LoyaleConfig.shared.token)"
    }
    
    public var schemeId: String {
        return LoyaleConfig.shared.schemeId
    }
    
    public var baseURL: URL {
        return LoyaleConfig.shared.baseLoyaltyURL
    }
    
    public var path: String {
        switch self {
        case .postMessage:
            return "api/Message"
        case .contactMatch:
            return "api/Contact/match"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .postMessage:
            return .post
        case .contactMatch:
            return .post
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .postMessage(let message):
            return .requestParametersAndHeaders(bodyParameters: message.parameters(), bodyEncoding: .jsonEncoding, urlParameters: nil, additionHeaders: headers)
        case .contactMatch(let contacts):
            return .requestParametersAndHeaders(bodyParameters: ["Contacts": contacts], bodyEncoding: .jsonEncoding, urlParameters: nil, additionHeaders: headers)
        }
    }
    
    public var headers: HTTPHeaders? {
        switch self {
        default:
            return ["Scheme": schemeId, "Authorization": loyaleToken]
        }
    }
}
