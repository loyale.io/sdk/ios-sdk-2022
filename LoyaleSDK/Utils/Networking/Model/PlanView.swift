//
//  PlanView.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public struct PlanView {
    
    public let id: String
    public var name: String?
    public var monthlyBasePrice: Int
    public var yearlyBasePrice: Int
    public var agents: Int
    public var customers: Int
    public var pushNotifications: Int
    public var points: Bool
    public var affiliates: Bool
    public var coupons: Bool
    public var pointsExpiry: Bool
    public var hidden: Bool
    public var multipleGroups: Bool
    public var multipleBrands: Bool
    public var api: Bool
    public var walletApp: Bool
    public var posApp: Bool
    public var integrations: Int
}

extension PlanView: Decodable {
   
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case monthlyBasePrice
        case yearlyBasePrice
        case agents
        case customers
        case pushNotifications
        case points
        case affiliates
        case coupons
        case pointsExpiry
        case hidden
        case multipleGroups
        case multipleBrands
        case api
        case walletApp
        case posApp
        case integrations
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.monthlyBasePrice = try container.decode(Int.self, forKey: .monthlyBasePrice)
        self.yearlyBasePrice = try container.decode(Int.self, forKey: .yearlyBasePrice)
        self.agents = try container.decode(Int.self, forKey: .agents)
        self.customers = try container.decode(Int.self, forKey: .customers)
        self.pushNotifications = try container.decode(Int.self, forKey: .pushNotifications)
        self.points = try container.decode(Bool.self, forKey: .points)
        self.affiliates = try container.decode(Bool.self, forKey: .affiliates)
        self.coupons = try container.decode(Bool.self, forKey: .coupons)
        self.pointsExpiry = try container.decode(Bool.self, forKey: .pointsExpiry)
        self.hidden = try container.decode(Bool.self, forKey: .hidden)
        self.multipleGroups = try container.decode(Bool.self, forKey: .multipleGroups)
        self.multipleBrands = try container.decode(Bool.self, forKey: .multipleBrands)
        self.api = try container.decode(Bool.self, forKey: .api)
        self.walletApp = try container.decode(Bool.self, forKey: .walletApp)
        self.posApp = try container.decode(Bool.self, forKey: .posApp)
        self.integrations = try container.decode(Int.self, forKey: .integrations)
    }
}
