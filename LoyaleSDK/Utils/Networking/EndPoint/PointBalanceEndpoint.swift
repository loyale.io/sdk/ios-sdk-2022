//
//  PointBalanceEndpoint.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public enum PointBalanceEndpoint {
    case getPointBalance
}

extension PointBalanceEndpoint: EndPointType {
   
    public var loyaleToken: String {
        return "Bearer \(LoyaleConfig.shared.token)"
    }
    
    public var schemeId: String {
        return LoyaleConfig.shared.schemeId
    }
    
    public var baseURL: URL {
        return LoyaleConfig.shared.baseLoyaltyURL
    }
    
    public var path: String {
        switch self {
        case .getPointBalance:
            return "api/PointBalance"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .getPointBalance:
            return .get
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .getPointBalance:
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: headers)
        }
    }
    
    public var headers: HTTPHeaders? {
        switch self {
        default:
            return ["Scheme": schemeId, "Authorization": loyaleToken]
        }
    }
}
