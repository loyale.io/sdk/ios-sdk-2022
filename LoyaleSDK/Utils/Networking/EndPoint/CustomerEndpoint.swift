//
//  CustomerEndpoint.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public enum CustomerEndpoint {
    case createCustomer(customer: CustomerInsertView, additionalFiedlds: [AdditionalCustomerFieldsResultView])
    case updateCustomer(customer: CustomerResultView)
    case getCustomerById(_ id: String)
    case postFirebaseToken(token: String, customerId: String)
    case deleteFirebaseToken(token: String)
    case uploadProfilePicture(customerId: String, boundary: String)
    case leaveScheme(schemeId: String, customerId: String)
    case joinScheme(schemeId: String, customerId: String)
    case suspend(customerId: String)
    case getСustomerWithAdditional(customerId: String)
}

extension CustomerEndpoint: EndPointType {
    
    public var loyaleToken: String {
        return "Bearer \(LoyaleConfig.shared.token)"
    }
    
    public var schemeId: String {
        return LoyaleConfig.shared.schemeId
    }
    
    public var baseURL: URL {
        return LoyaleConfig.shared.baseLoyaltyURL
    }
    
    public var path: String {
        switch self {
        case .createCustomer:
            return "api/Customer/PostAdditional"
        case .updateCustomer:
            return "api/Customer/PutAdditional"
        case .getCustomerById(let customerId):
            return "api/Customer/\(customerId)"
        case .postFirebaseToken:
            return "api/PushToken"
        case .deleteFirebaseToken:
            return "api/PushToken"
        case .uploadProfilePicture:
            return "api/Upload/ProfilePicture"
        case .leaveScheme(let schemeId, _):
            return "api/Customer/LeaveScheme/\(schemeId)"
        case .joinScheme(let schemeId, _):
            return "api/Customer/JoinScheme/\(schemeId)"
        case .suspend(let customerId):
            return "api/Customer/Suspend/\(customerId)"
        case .getСustomerWithAdditional:
            return "/api/Customer/GetAdditional"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .createCustomer, .postFirebaseToken, .leaveScheme, .joinScheme, .uploadProfilePicture:
            return .post
        case .updateCustomer, .suspend:
            return .put
        case .getCustomerById, .getСustomerWithAdditional:
            return .get
        case .deleteFirebaseToken:
            return .delete
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .createCustomer(let customer, let additionalFiedlds):
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let mappedAdditionalFiedlds = try! encoder.encode(additionalFiedlds)
            let additionalFieldsJSON = try! JSONSerialization.jsonObject(with: mappedAdditionalFiedlds, options: .allowFragments) as! [String : Any]
            return .requestParametersAndHeaders(bodyParameters: ["customer": customer.parameters(), "additionalFields": additionalFieldsJSON], bodyEncoding: .jsonEncoding, urlParameters: nil, additionHeaders: headers)
        case .updateCustomer(let  customer):
            return .requestParametersAndHeaders(bodyParameters: ["customer": customer.parameters()], bodyEncoding: .jsonEncoding, urlParameters: nil, additionHeaders: headers)
        case .getCustomerById:
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: headers)
        case .postFirebaseToken(let token, let customerId):
            return .requestParametersAndHeaders(bodyParameters: ["customerId": customerId, "token":  token] , bodyEncoding: .jsonEncoding, urlParameters: nil, additionHeaders: headers)
        case .deleteFirebaseToken(let token):
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: ["token": token], additionHeaders: headers)
        case .uploadProfilePicture(let customerId, _):
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: ["customerId": "\(customerId)"], additionHeaders: headers)
        case .leaveScheme(_, let customerId), .joinScheme(_, let customerId):
            return .requestParameters(bodyParameters: nil, bodyEncoding: .jsonEncoding, urlParameters: ["customerId": customerId])
        case .suspend:
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .jsonEncoding, urlParameters: nil, additionHeaders: headers)
        case .getСustomerWithAdditional(let customerId):
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: ["customerId": customerId], additionHeaders: headers)
        }
    }
    
    public var headers: HTTPHeaders? {
        switch self {
        case .leaveScheme, .joinScheme:
            return nil
        case .uploadProfilePicture( _, let boundary):
            return ["Scheme": schemeId, "Authorization": loyaleToken, "Content-Type": "multipart/form-data; boundary=\(boundary)"]
        default:
            return ["Scheme": schemeId, "Authorization": loyaleToken]
        }
    }
}
