//
//  Message.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public struct Message {
    
    public let customerId: String
    public let status: Int
    public let text: String
    public let attachments: [String]
}

extension Message: Encodable {
    
    private enum CodingKeys: String, CodingKey {
        case customerId
        case status
        case text
        case attachments
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.customerId, forKey: .customerId)
        try container.encode(self.status, forKey: .status)
        try container.encode(self.text, forKey: .text)
        try container.encode(self.attachments, forKey: .attachments)
    }
}

extension Message {
    
    public func parameters() -> Parameters {
        do{
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let mappedUser = try? encoder.encode(self)
            let json = try JSONSerialization.jsonObject(with: mappedUser!, options: .allowFragments) as? [String : Any]
            return json ?? [String: Any]()
        } catch {
            print(error)
            return [String: Any]()
        }
    }
}
