//
//  ExtNSMutableData.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

extension NSMutableData {
  func appendString(_ string: String) {
    if let data = string.data(using: .utf8) {
      self.append(data)
    }
  }
}
