//
//  HelpersRouter.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public class HelpersRouter {
    
    private let router = Router<HelpersEndpoint>()

    public func getTowns(filteredBy countryCode: String, completion: @escaping (_ model: [Town]?, _ error: String?)->()) {
        router.getData(.getTowns(countryCode: countryCode)) { (data: [Town]?, error: String?) in
            completion(data, error)
        }
    }
    
    public func getCountries(completion: @escaping (_ model: [Country]?, _ error: String?)->()) {
        router.getData(.getCountries) { (data: [Country]?, error: String?) in
            completion(data, error)
        }
    }
    
    public func getCurrency(filteredBy filters: String, completion: @escaping (_ model: [Currency]?, _ error: String?)->()) {
        router.getData(.getCurrency(filters: filters)) { (data: [Currency]?, error: String?) in
            completion(data, error)
        }
    }
    
    public func getMobileCodes(completion: @escaping (_ model: [MobileCode]?, _ error: String?)->()) {
        router.getData(.getMobileCodes) { (data: [MobileCode]?, error: String?) in
            completion(data, error)
        }
    }
}
