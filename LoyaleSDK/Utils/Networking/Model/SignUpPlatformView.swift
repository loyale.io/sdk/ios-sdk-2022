//
//  SignUpPlatformView.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public struct SignUpPlatformView {
    
    public var name: String?
    public var version: String?
    public var device: String?
    public var appName: String?
    public var appVersion: String?
    public var comments: String?
}

extension SignUpPlatformView {
    
    public init(name: String, version: String, device: String, appName: String, appVersion: String, comments: String) {
        self.name = name
        self.version = version
        self.device = device
        self.appName = appName
        self.appVersion = appVersion
        self.comments = comments
    }
}

extension SignUpPlatformView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case name
        case version
        case device
        case appName
        case appVersion
        case comments
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.name = try? container.decode(String.self, forKey: .name)
        self.version = try? container.decode(String.self, forKey: .version)
        self.device = try? container.decode(String.self, forKey: .device)
        self.appName = try? container.decode(String.self, forKey: .appName)
        self.appVersion = try? container.decode(String.self, forKey: .appVersion)
        self.comments = try? container.decode(String.self, forKey: .comments)
    }
}
