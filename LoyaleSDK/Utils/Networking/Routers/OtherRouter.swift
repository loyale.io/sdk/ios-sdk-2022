//
//  OtherRouter.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public class OtherRouter {
    
    private let router = Router<OtherEndpoint>()
    
    public func postMessage(_ message: Message, completion: @escaping (_ success: Bool, _ error: String?)->()) {
        router.simpleRequest(.postMessage(message: message)) { success, error in
            completion(success, error)
        }
    }
}
