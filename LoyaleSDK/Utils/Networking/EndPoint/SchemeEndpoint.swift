//
//  SchemeEndpoint.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public enum SchemeEndpoint {
    case getSchemeById(_ id: String)
}

extension SchemeEndpoint: EndPointType {
    
    public var loyaleToken: String {
        return "Bearer \(LoyaleConfig.shared.token)"
    }
    
    public var schemeId: String {
        return LoyaleConfig.shared.schemeId
    }
    
    public var baseURL: URL {
        return LoyaleConfig.shared.baseLoyaltyURL
    }
    
    public var path: String {
        switch self {
        case .getSchemeById(let schemeId):
            return "api/Scheme/\(schemeId)"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .getSchemeById:
            return .get
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .getSchemeById:
            return .requestParameters(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil)
        }
    }
    
    public var headers: HTTPHeaders? {
        switch self {
        default:
            return ["Scheme": schemeId, "Authorization": loyaleToken]
        }
    }
}
