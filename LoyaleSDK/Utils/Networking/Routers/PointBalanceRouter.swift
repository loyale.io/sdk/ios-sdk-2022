//
//  PointBalanceRouter.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public class PointBalanceRouter {
    
    private let router = Router<PointBalanceEndpoint>()
    
    public func getPointBalance(completion: @escaping (_ model: PointBalanceResultView?, _ error: String?)->()) {
        router.getData(.getPointBalance) { (array: [PointBalanceResultView]?, error: String?) in
            if let array = array, array.count > 0 {
                completion(array[0], error)
            } else {
                completion(nil, error)
            }
        }
    }
}
