//
//  LevelRouter.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public class LevelRouter {
    
    private let router = Router<LevelEndpoint>()
    
    public func getLevels(filteredBy filters: String?, sorts: String?, page: Int?, pageSize: Int?, completion: @escaping (_ model: [LevelResultView]?, _ error: String?)->()) {
        router.getData(.getLevels(filters: filters, sorts: sorts, page: page, pageSize: pageSize)) { (model: [LevelResultView]?, error: String?) in
            completion(model, error)
        }
    }
    
    public func getLevel(byId id: String, completion: @escaping (_ model: LevelResultView?, _ error: String?)->()) {
        router.getData(.getLevelById(id)) { (model: LevelResultView?, error: String?) in
            completion(model, error)
        }
    }
}
