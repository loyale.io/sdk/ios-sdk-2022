//
//  CustomerResultview.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public class CustomerResultView: BaseCustomer {

    public var id: String
    public var name: String?
    public var barCode: String?
    public var levels: [LevelResultView]?
    public var schemes: [SchemeResultView]?
    public var createdDate: String?
    public var updatedDate: String?
    public var lastConnectedDate: String?
    public var lastTransactionDate: String?
    public var alerts: [String]?
    public var additionalCustomerFields: [AdditionalCustomerFieldsResultView]?
    public var onHold: Bool?
    public var deleted: Bool?
    public var emailVerified: Bool?
    public var mobileNumberVerified: Bool?
    public var signUpPlatform: String?
    public var isChild: Bool?
    public var parent: CustomerResultView?
    public var balance: Balance?
    public var customerDevices: [CustomerDevice]?
    
    public var barcodeUrl: URL? {
        guard let barcodeStr = barCode else { return nil }
        let baseUrlStr = LoyaleConfig.shared.baseLoyaltyURL.absoluteString
        let urlStr = "\(baseUrlStr)/api/BarCode/barcode?barcode=\(barcodeStr)"
        return URL(string: urlStr)
    }
    
    public var qrСodeUrl: URL? {
        guard let barcodeStr = barCode else { return nil }
        let baseUrlStr = LoyaleConfig.shared.baseLoyaltyURL.absoluteString
        let urlStr = "\(baseUrlStr)/api/BarCode/qrcode?qrcode=\(barcodeStr)"
        return URL(string: urlStr)
    }
    
    override public init() {
        self.id = ""
        super.init()
    }
    
    private init (firstName: String?, lastName: String?, dateOfBirth: String?, email: String?, areaCode: String?, mobileNumber: String?, gender: Gender?, town: String?, state: String?, postCode: String?, addressLine1: String?, addressLine2: String?, country: String?, marketingSub: Bool?, profileImageUrl: String?, externalRefId: String?, id: String, name: String?, barCode: String?, levels: [LevelResultView]?, schemes: [SchemeResultView]?, createdDate: String?, updatedDate: String?, lastConnectedDate: String?, lastTransactionDate: String?, alerts: [String]?, additionalCustomerFields: [AdditionalCustomerFieldsResultView]?, onHold: Bool?, deleted: Bool?, emailVerified: Bool?, mobileNumberVerified: Bool?, signUpPlatform: String?, isChild: Bool?, parent: CustomerResultView?, balance: Balance?, customerDevices: [CustomerDevice]?) {
        self.id = id
        super.init()
        self.firstName = firstName
        self.lastName = lastName
        self.dateOfBirth = dateOfBirth
        self.email = email
        self.areaCode = areaCode
        self.mobileNumber = mobileNumber
        self.gender = gender
        self.town = town
        self.state = state
        self.postCode = postCode
        self.addressLine1 = addressLine1
        self.addressLine2 = addressLine2
        self.country = country
        self.marketingSub = marketingSub
        self.profileImageUrl = profileImageUrl
        self.externalRefId = externalRefId
        self.name = name
        self.barCode = barCode
        self.levels = levels
        self.schemes = schemes
        self.createdDate = createdDate
        self.updatedDate = updatedDate
        self.lastConnectedDate = lastConnectedDate
        self.lastTransactionDate = lastTransactionDate
        self.alerts = alerts
        self.additionalCustomerFields = additionalCustomerFields
        self.onHold = onHold
        self.deleted = deleted
        self.emailVerified = emailVerified
        self.mobileNumberVerified = mobileNumberVerified
        self.signUpPlatform = signUpPlatform
        self.isChild = isChild
        self.parent = parent
        self.balance = balance
        self.customerDevices = customerDevices
    }
   
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case barCode
        case levels
        case schemes
        case createdDate
        case updatedDate
        case lastConnectedDate
        case lastTransactionDate
        case alerts
        case additionalCustomerFields
        case onHold
        case deleted
        case emailVerified
        case mobileNumberVerified
        case signUpPlatform
        case isChild
        case parent
        case balance
        case customerDevices
    }
    
    public required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.barCode = try? container.decode(String.self, forKey: .barCode)
        self.levels = try? container.decode([LevelResultView].self, forKey: .levels)
        self.schemes = try? container.decode([SchemeResultView].self, forKey: .schemes)
        self.createdDate = try? container.decode(String.self, forKey: .createdDate)
        self.updatedDate = try? container.decode(String.self, forKey: .updatedDate)
        self.lastConnectedDate = try? container.decode(String.self, forKey: .lastConnectedDate)
        self.lastTransactionDate = try? container.decode(String.self, forKey: .lastTransactionDate)
        self.alerts = try? container.decode([String].self, forKey: .alerts)
        self.additionalCustomerFields = try? container.decode([AdditionalCustomerFieldsResultView].self, forKey: .additionalCustomerFields)
        self.onHold = try? container.decode(Bool.self, forKey: .onHold)
        self.deleted = try? container.decode(Bool.self, forKey: .deleted)
        self.emailVerified = try? container.decode(Bool.self, forKey: .emailVerified)
        self.mobileNumberVerified = try? container.decode(Bool.self, forKey: .mobileNumberVerified)
        self.signUpPlatform = try? container.decode(String.self, forKey: .signUpPlatform)
        self.isChild = try? container.decode(Bool.self, forKey: .isChild)
        self.parent = try? container.decode(CustomerResultView.self, forKey: .parent)
        self.balance = try? container.decode(Balance.self, forKey: .balance)
        self.customerDevices = try? container.decode([CustomerDevice].self, forKey: .customerDevices)
        try super.init(from: decoder)
    }
    
    public override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.id, forKey: .id)
        try super.encode(to: encoder)
    }
}

extension CustomerResultView {
    
    public func parameters() -> Parameters {
        do{
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let mappedUser = try? encoder.encode(self)
            let json = try JSONSerialization.jsonObject(with: mappedUser!, options: .allowFragments) as? [String : Any]
            return json ?? [String: Any]()
        } catch {
            print(error)
            return [String: Any]()
        }
    }
}

extension CustomerResultView: NSCopying {
    
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = CustomerResultView(firstName: firstName, lastName: lastName, dateOfBirth: dateOfBirth, email: email, areaCode: areaCode, mobileNumber: mobileNumber, gender: gender, town: town, state: state, postCode: postCode, addressLine1: addressLine1, addressLine2: addressLine2, country: country, marketingSub: marketingSub, profileImageUrl: profileImageUrl, externalRefId: externalRefId, id: id, name: name, barCode: barCode, levels: levels, schemes: schemes, createdDate: createdDate, updatedDate: updatedDate, lastConnectedDate: lastConnectedDate, lastTransactionDate: lastTransactionDate, alerts: alerts, additionalCustomerFields: additionalCustomerFields, onHold: onHold, deleted: deleted, emailVerified: emailVerified, mobileNumberVerified: mobileNumberVerified, signUpPlatform: signUpPlatform, isChild: isChild, parent: parent, balance: balance, customerDevices: customerDevices)
        return copy
    }
}
