//
//  FranchiseRouter.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public class FranchiseRouter {
    
    private let router = Router<FranchiseEndpoint>()
    
    public func getFranchises(filteredBy filters: String?, sorts: String?, page: Int?, pageSize: Int?, completion: @escaping (_ model: [FranchiseResultView]?, _ error: String?)->()) {
        router.getData(.getFranchises(filters: filters, sorts: sorts, page: page, pageSize: pageSize)) { (model: [FranchiseResultView]?, error: String?) in
            completion(model, error)
        }
    }
    
    public func getFranchise(byId id: String, completion: @escaping (_ model: FranchiseResultView?, _ error: String?)->()) {
        router.getData(.getFranchiseById(id)) { (model: FranchiseResultView?, error: String?) in
            completion(model, error)
        }
    }
}
