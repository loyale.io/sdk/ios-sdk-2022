//
//  CouponResultView.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public struct CouponResultView {
    
    public let id: String
    public let schemeId: String
    public let name: String
    public let description: String?
    public let valid: Bool
    public let initialNumberOfUses: Int?
    public var from: String
    public var to: String?
    public var until: String
    public let imageUrl: String?
    public var updatedDate: String?
    public var createdDate: String?
    public let outletId: [String]?
    public let externalReference: String?
    public let platform: String?
    public let isDyanmic: Bool
    public let dynamicDays: Int?

}

extension CouponResultView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case schemeId
        case name
        case description
        case valid
        case initialNumberOfUses
        case from
        case to
        case until
        case imageUrl
        case updatedDate
        case createdDate
        case outletId
        case externalReference
        case platform
        case isDyanmic
        case dynamicDays
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.schemeId = try container.decode(String.self, forKey: .schemeId)
        self.name = try container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.valid = try container.decode(Bool.self, forKey: .valid)
        self.initialNumberOfUses = try? container.decode(Int.self, forKey: .initialNumberOfUses)
        self.from = try container.decode(String.self, forKey: .from)
        self.to = try? container.decode(String.self, forKey: .to)
        self.until = try container.decode(String.self, forKey: .until)
        self.imageUrl = try? container.decode(String.self, forKey: .imageUrl)
        self.updatedDate = try? container.decode(String.self, forKey: .updatedDate)
        self.createdDate = try? container.decode(String.self, forKey: .createdDate)
        self.outletId = try? container.decode([String].self, forKey: .outletId)
        self.externalReference = try? container.decode(String.self, forKey: .externalReference)
        self.platform = try? container.decode(String.self, forKey: .platform)
        self.isDyanmic = try container.decode(Bool.self, forKey: .isDyanmic)
        self.dynamicDays = try? container.decode(Int.self, forKey: .dynamicDays)
        
    }
}
