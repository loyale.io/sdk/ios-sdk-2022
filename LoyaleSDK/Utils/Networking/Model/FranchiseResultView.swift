//
//  FranchiseResultView.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public struct FranchiseResultView {
    
    public let id: String
    public var name: String?
    public var description: String?
    public var schemeId: String
    public var imageUrl: URL?
    public var outlets: [OutletResultView]?
    public var categories: [CategoryResultView]?
    public var hidden: Bool
    public var createdDate: String?
    public var updatedDate: String?
    public var imageGallery: [String]?
    public var categoriesIds: [String]?
}

extension FranchiseResultView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case schemeId
        case imageUrl
        case outlets
        case hidden
        case categories
        case createdDate
        case updatedDate
        case imageGallery
        case categoriesIds
    }
    
    public init(from decoder: Decoder) throws {
       
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.schemeId = try container.decode(String.self, forKey: .schemeId)
        self.hidden = try container.decode(Bool.self, forKey: .hidden)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.outlets = try? container.decode([OutletResultView].self, forKey: .outlets)
        self.imageGallery = try? container.decode([String].self, forKey: .imageGallery)
        self.categoriesIds = try? container.decode([String].self, forKey: .categoriesIds)
        self.categories = try? container.decode([CategoryResultView].self, forKey: .categories)

        if let str = try? container.decode(String.self, forKey: .imageUrl), let url = URL(string: str) {
            imageUrl = url
        }
        
        self.updatedDate = try? container.decode(String.self, forKey: .updatedDate)
        self.createdDate = try? container.decode(String.self, forKey: .createdDate)
    }
}
