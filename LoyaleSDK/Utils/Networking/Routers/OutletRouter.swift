//
//  OutletRouter.swift
//  LoyaleSDK
//
//  Created by Nirabaz on 01.06.2022.
//

import Foundation

public class OutletRouter {
    
    private let router = Router<OutletEndpoint>()
    
    public func getOutlets(filteredBy filters: String?, sorts: String?, page: Int?, pageSize: Int?, completion: @escaping (_ model: [OutletResultView]?, _ error: String?)->()) {
        router.getData(.getOutlets(filters: filters, sorts: sorts, page: page, pageSize: pageSize)) { (model: [OutletResultView]?, error: String?) in
            completion(model, error)
        }
    }
    
    public func getOutlet(byId id: String, completion: @escaping (_ model: OutletResultView?, _ error: String?)->()) {
        router.getData(.getOutletById(id)) { (model: OutletResultView?, error: String?) in
            completion(model, error)
        }
    }
}
