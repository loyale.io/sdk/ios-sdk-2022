Pod::Spec.new do |s|

s.platform = :ios
s.ios.deployment_target = '12.0'
s.name = "Loyale"
s.summary = "Loyale lets a user use the base LoyaleAPI calls"
s.requires_arc = true
s.version = '1.0'
s.license = { :type => "MIT", :file => "LICENSE.md" }
s.author = { "Think" => "info@think.mt" }
s.homepage = "https://gitlab.com/loyale.io/sdk/ios-sdk-2022"
s.source = { :git => "https://gitlab.com/loyale.io/sdk/ios-sdk-2022.git", :tag => "{s.version}" }
s.source_files = "**/*.{swift}"
s.swift_version = "5.0"

end

